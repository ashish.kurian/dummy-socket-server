const express = require('express');
const socket = require('socket.io');
const rs = require('randomstring');
const app = express();

const nts = [
  "badge",
  "notification"
]

const types = [
  'lead-add',
  'task-add',
  'property-approval',
  'property-rejection',
  'lead-add',
  'task-add',
  'task-edit',
  'transaction-add',
  'transaction-edit',
  'property-lead'
]

const messages = [
  'Someone killed Ram',
  'Eat a carrot',
  'Make a sandwich',
  'Kill cockroaches',
  'Watch birds',
  'Wash clothes',
  'Clean the bed',
  'Clean the room',
  'Make cool coffee',
  'Watch cool movies',
  'Travel the world'
]

// async function justDoIt(io) {
//   await setTimeout(() => {
//     io.sockets.emit('notif', getRandomContent())
//   }, 3000); // every 3 seconds
// } 

function getRandomContent() {
  let ml = messages.length;
  let mindex = Math.floor(Math.random() * ml);
  let m = messages[mindex];

  let tl = types.length;
  let tindex = Math.floor(Math.random() * tl);
  let t = types[tindex];
  
  let ntl = nts.length;
  let ntindex = Math.floor(Math.random() * ntl);
  let nt = nts[ntindex];

  return  {
    "_id": rs.generate(14),
    "seen" : {
      "flag": false,
      "time": "2018-02-16 12:12:10"
    },
    "delivered" : {
      "flag": false,
      "time": "2018-02-16 12:12:10"
    },
    "action_type" : t,
    "notification_type": nt,
    "msg": {
      "url": "http://propok.com/prop/121212w12",
      "content": m,
      "time": "2018-02-16 12:12:10"
    }
  };

}

const server = app.listen(4000, () => {
	console.log('listening on port 4000');
})


// app setup
app.use(express.static('public'));

// socket setup
const io = socket(server);

io.on('connection', async (sock) => {

  console.log('connection -> ', sock.id);
  
  setInterval(() => {
    io.emit('notif', getRandomContent());
  }, 3000);

  // for(let i=0; i< 3; i++) {
  //   setTimeout(() => {
  //     io.sockets.emit('notif', getRandomContent())
  //   }, 3000); // every 3 seconds
  // }
  
});
